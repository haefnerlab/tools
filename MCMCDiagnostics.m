function statstbl = MCMCDiagnostics(chains,variablenames,maxlag)
%MCMCDIAGNOSTICS copy of Matlab's internal diagnostics@stats.mcmc.impl.MCMCImpl 

if ~iscell(chains), chains = {chains}; end

% 2. How many variable? How many chains?
P = size(chains{1}, 1);
M = length(chains);

% 3. rhat below is a P-by-1 vector.
rHat = stats.mcmc.utils.computeRHat(chains);

% 4. Compute effective sample size for each variable for a
% given chain and then add up the values across chains.
ess = zeros(P,M);
for i = 1:M
    ess(:,i) = stats.mcmc.utils.computeESS(chains{i},maxlag);
end
ess = sum(ess,2);

% 5. Get posterior mean and variance using all samples.
chains     = chains(:)';
samples    = cell2mat(chains);
muHat      = mean(samples,2);
sigma2Hat  = var(samples,0,2);
quantile5  = quantile(samples,0.05,2);
quantile95 = quantile(samples,0.95,2);

% 6. Estimate the monte carlo variance - this is the variance
% of muHat.
varMuHat = sigma2Hat./ess;

% 7. Put stuff into a table.
statstbl      = table();
statstbl.Name = variablenames;
statstbl.Mean = muHat;
statstbl.MCSE = sqrt(varMuHat);
statstbl.SD   = sqrt(sigma2Hat);
statstbl.Q5   = quantile5;
statstbl.Q95  = quantile95;
statstbl.ESS  = ess;
statstbl.RHat = rHat;
end