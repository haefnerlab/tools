function [fit, tbl, exitflag] = stanWrapper(varargin)

fit = stan(varargin{:});

try
    pause(1);
    laststatus = cell(1,4);
    while fit.is_running()
        pause(1);
        for iproc=1:length(fit.processes)
            status = strip(fit.processes(iproc).stdout{end});
            if ~strcmpi(status, laststatus{iproc})
                fprintf('Chain %d status: %s\n', iproc, status);
                laststatus{iproc} = status;
            end
        end
    end
    pause(1);
    exitflag = [fit.processes.exitValue];
    [~,tbl] = fit.print();
catch e
    warning('STANWRAPPER FAILED TO MONITOR CHAINS. Error message: %s', getReport(e));
    exitflag = -1;
    
    fprintf('=== Last STAN Messages ===\n');
    for iproc=1:length(fit.processes)
        if ~isempty(fit.processes(iproc).stdout)
            fprintf('Chain %d :: %s\n', iproc, fit.processes(iproc).stdout{iproc});
        end
    end
end
end