function h = covEllipse(mu, cov, color, nstd, varargin)
%COVELLIPSE draw mean + covariance as an ellipse. 
if nargin < 4 || isempty(nstd), nstd = 1; end

% Start with the contour for a normal at zero with cov = eye
t = linspace(0, 2*pi, 101)';
xy = nstd*[cos(t) sin(t)];

% Scale/skew unit circle by cholesky factor of cov (think of points on the circle as samples from
% a unit gaussian that are being transformed into samples of a gaussian with covariance 'cov')
L = cholcov(cov);
xy = xy * L;

% Shift by mu
xy = xy + mu;

% Fill patch passing extra arguments through
h = patch(xy(:,1), xy(:,2), color, varargin{:});

end