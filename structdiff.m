function diff = structdiff(S1, S2)

diff = S1;
fields = fieldnames(S1);
for iField=1:length(fields)
    if isequaln(S1.(fields{iField}), S2.(fields{iField}))
        % Check equality with 'isequaln' so that nan == nan is 'true'
        diff = rmfield(diff, fields{iField});
    elseif isstruct(S1.(fields{iField}))
        diff.(fields{iField}) = structdiff(S1.(fields{iField}), S2.(fields{iField}));
    end
end

end