%% Simple demonstration - test density and sampling

mu1 = randn();
mu2 = randn();
sig1 = exp(randn());
sig2 = exp(randn());
pi1 = rand();
pi2 = 1-pi1;

mog_p = mog.create([mu1 mu2], [sig1 sig2], [pi1 pi2]);

% Draw lots of samples and estimate their density
xsamples = mog.sample(mog_p, 10000);
[f,xvals] = ksdensity(xsamples);
f = f / sum(f);

% Compute true pdf on xvals
truepdf = normpdf(xvals, mu1, sig1)*pi1 + normpdf(xvals, mu2, sig2)*pi2;
truepdf = truepdf/sum(truepdf);

% Analytic density with mog.pdf
pdf1 = mog.pdf(xvals, mog_p);
pdf1 = pdf1 / sum(pdf1);

pdf2 = mog.pdf(xvals, mog_p, true);

% Analytic density with mog.logpdf
logpdf = mog.logpdf(xvals', mog_p)';
pdf3 = exp(logpdf-max(logpdf));
pdf3 = pdf3 / sum(pdf3);

% Comparisons
assert(all(truepdf == pdf1), 'Mismatch between true pdf and mog.pdf');
tvd = dot(truepdf, abs(truepdf-f));
assert(tvd < 5e-3, 'Mismatch between samples density and true pdf');
assert(all(truepdf == pdf2), 'Mismatch between true pdf and auto-normalized pdf');
tvd = dot(truepdf, abs(truepdf-pdf3));
assert(tvd < 1e-9, 'Mismatch between true pdf and mog.logpdf');

disp('passed');

%% Test vectorized version with multiple mixtures

clear;

% Create M distributions, each a mixture of 3 gaussians
M = randi(10);
for m=M:-1:1
    mog_p(m,:) = mog.create(randn(1,3), exp(randn(1,3)), rand(1,3));
end

xsamples = mog.sample(mog_p, 10000);
assert(all(size(xsamples) == [M 10000]));

% Each row of 'xsamples' should match the density of the corresponding row of mog_p
for m=M:-1:1
    [f(m,:),xvals(m,:)] = ksdensity(xsamples(m, :));
    f(m,:) = f(m,:) / sum(f(m,:));
    pdf_per(m,:) = mog.pdf(xvals(m,:), mog_p(m,:), true);
end

pdf_all = mog.pdf(xvals, mog_p, true);
logpdf_all = mog.logpdf(xvals, mog_p);
log2pdf_all = exp(logpdf_all-max(logpdf_all, [], 2));
log2pdf_all = log2pdf_all ./ sum(log2pdf_all, 2);

assert(all(pdf_per(:) == pdf_all(:)), 'Mismatch between per-row PDF and vectorized PDF');
tvd = sum(pdf_all.*abs(log2pdf_all-pdf_all), 2);
assert(all(tvd < 1e-9), 'Mismatch between per-row PDF and vectorized log PDF');
tvd = sum(pdf_all.*abs(f-pdf_all), 2);
assert(all(tvd < 5e-3), 'Mismatch between vectorized samples density and true PDF');

disp('passed');