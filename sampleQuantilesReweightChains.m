function [w, quantiles, pmf, edges] = sampleQuantilesReweightChains(values, loglikes, quants, edges)
%SAMPLEQUANTILESREWEIGHTCHAINS reweight MCMC chains according to their average likelihood. Also
%compute requested quantiles and discrete PMF adjusted according to weights.
%
% weights = SAMPLEQUANTILESREWEIGHTCHAINS(values, loglikes) given cell array of sample values, i.e.
% values{i} = [nsamples x nparams] chain, return [ntotal x 1] weights on each individual sample.
% loglikes must similarly be either a cell array or [ntotal x 1] values of the log likelihood of
% each sample.
%
% [weights, qvalues] = SAMPLEQUANTILESREWEIGHTCHAINS(values, loglikes, quantiles) also computes
% quantiles, e.g. if 'quantiles' is [0.25 0.5 0.75] then 'qvalues' will be [nparams x 3], containing
% the 25%, 50%, and 75% quantiles of each parameter.
%
% [~, ~, pmf, edges] = SAMPLEQUANTILESREWEIGHTCHAINS(values, loglikes, quantiles, [edges]) returns
% an adjusted probability mass function (PMF) for each value. By default, 'edges' is 101 values
% linearly spaced from the min to the max of each paramter; to override this, pass the 'edges' in
% directly. The PMF will have size [nparams x size(edges,2)-1]. For instance, a histogram of the
% mass for the jth paramter can be plotted using
%    centers = (edges(j, 1:end-1) + edges(j, 2:end))/2;
%    bar(centers, pmf(j, :));

% default to median
if nargin < 3, quants = .5; end

if iscell(values)
    csizes = cellfun(@(val) size(val,1), values);
    values = vertcat(values{:});
end

if iscell(loglikes)
    if exist('csizes', 'var')
        assert(all(cellfun(@length, loglikes) == csizes), 'Mismatch in chain sizes!');
    else
        csizes = cellfun(@length, loglikes);
    end
    loglikes = vertcat(loglikes{:});
end

if ~exist('edges', 'var')
    for j=size(values,2):-1:1
        edges(j,:) = linspace(min(values(:,j)), max(values(:,j)), 101);
    end
end

%% Compute weight per chain, expand out to weight per sample
rescale_ll = mat2cell(loglikes - max(loglikes), csizes(:), 1);
mean_like = cellfun(@(ll) mean(exp(ll)), rescale_ll);
chain_w = mean_like / sum(mean_like); % weight per chain

w_idx = arrayfun(@(i) i*ones(csizes(i),1), 1:length(csizes), 'uniformoutput', false);
w_idx = vertcat(w_idx{:});

w = chain_w(w_idx); % weight per sample
w = w / sum(w);

%% Get quantiles and PMF
for j=size(values,2):-1:1
    [sortv, isrt] = sort(values(:,j), 1);
    cdf = cumsum(w(isrt));
    
    for iq=1:length(quants)
        quantiles(j,iq) = interp1q(cdf, sortv, quants(iq));
    end
    
    pmf(j,:) = diff(interp1q(sortv(:), cdf(:), edges(j,:)'));
end
end